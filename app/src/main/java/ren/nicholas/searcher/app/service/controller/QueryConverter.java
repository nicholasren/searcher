package ren.nicholas.searcher.app.service.controller;

import io.vavr.control.Either;
import ren.nicholas.searcher.app.service.model.Schemas;

import static io.vavr.control.Either.left;
import static java.lang.String.format;

public class QueryConverter {

    public static <T> Either<String, ren.nicholas.searcher.search.api.Query> from(Query<T> query) {
        var  field = query.getField();
        var type = query.getEntityType().type();
        boolean validField = Schemas.fieldsOf(type).exists(f -> f.equals(field));

        if (!validField) {
            return left(format("Invalid field %s", field));
        }
        var converted = new ren.nicholas.searcher.search.api.Query(query.getEntityType().name(), field, query.getTerm());
        return Either.right(converted);
    }
}
