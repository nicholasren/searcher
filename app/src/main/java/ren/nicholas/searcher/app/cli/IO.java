package ren.nicholas.searcher.app.cli;

import io.vavr.collection.List;
import org.beryx.textio.TextIO;
import org.beryx.textio.TextTerminal;
import ren.nicholas.searcher.app.service.controller.Query;
import ren.nicholas.searcher.app.service.model.EntityType;

class IO {
    private final TextTerminal<?> terminal;
    private final TextIO textIO;

    IO(final TextIO textIO) {
        this.textIO = textIO;
        this.terminal = textIO.getTextTerminal();
    }

    public void welcome() {
        terminal.println("###############################");
        terminal.println("#   Welcome to Zendesk search #");
        terminal.println("###############################");
        terminal.println();
    }

    public Action askForAction() {
        return textIO.newEnumInputReader(Action.class)
                .withDefaultValue(Action.SEARCH)
                .read("Please select a action:");
    }


    public void showQuery(final Query query) {
        terminal.printf("\nSearching %s for %s with a value of %s\n",
                query.getEntityType(),
                query.getField(),
                query.getTerm());
    }

    public void showResults(final List<String> results) {
        if (results.isEmpty()) {
            terminal.println("No result found");
        } else {
            results.forEach(terminal::println);
        }
    }

    public Query query() {
        EntityType entityType = askForEntityType();
        String field = askForField();
        String term = askForTerm();
        return new Query(entityType, field, term);
    }

    public Boolean askForContinue() {
        return textIO
                .newBooleanInputReader()
                .withDefaultValue(Boolean.TRUE)
                .read("Continue?");
    }

    void show(final Throwable t) {
        terminal.println("Something Bad Happened, Press 'Ctrl + C' to quit!");
        t.printStackTrace();
    }

    void show(final String message) {
        terminal.println(message);
    }

    public EntityType askForEntityType() {
        return textIO.newEnumInputReader(EntityType.class)
                .withDefaultValue(EntityType.USER)
                .read("Please tell us what you want to search:");
    }

    public void dispose() {
        textIO.dispose();
    }

    private String askForField() {
        return textIO.newStringInputReader()
                .read("Enter search field");
    }

    private String askForTerm() {
        return textIO.newStringInputReader()
                .withMinLength(0)
                .read("Enter search term, (Press 'Enter' to search for empty value)");
    }
}
