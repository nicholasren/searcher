package ren.nicholas.searcher.app.service.extractor;

import ren.nicholas.searcher.search.api.Document;

import java.util.function.Function;

public interface Extractor<T> extends Function<Document, T> {
}
