package ren.nicholas.searcher.app.service;

import io.vavr.collection.List;
import io.vavr.control.Either;
import ren.nicholas.searcher.app.service.controller.Query;
import ren.nicholas.searcher.app.service.controller.SchemaController;
import ren.nicholas.searcher.app.service.controller.SearchController;
import ren.nicholas.searcher.app.service.model.EntityType;

public class SearchService {

    private final SearchController searchController;
    private final SchemaController schemaController;

    SearchService(final SearchController searchController, final SchemaController schemaController) {
        this.searchController = searchController;
        this.schemaController = schemaController;
    }

    public Either<String, List<String>> by(final Query query) {
        return searchController.by(query);
    }

    public String schemaFor(final EntityType entityType) {
        return schemaController.of(entityType);
    }
}
