package ren.nicholas.searcher.app.service.controller;

import ren.nicholas.searcher.app.service.model.EntityType;
import ren.nicholas.searcher.app.service.model.Schemas;
import ren.nicholas.searcher.app.service.presenter.Presenter;

public class SchemaController {

    public <T> String of(final EntityType entityType) {
        Presenter<T> presenter = Presenter.of(entityType.type());
        return presenter.show(Schemas.fieldsOf(entityType.type()));
    }
}
