package ren.nicholas.searcher.app.service.model;

public class TicketBuilder {
    private String id;
    private String url;
    private String externalId;
    private String createdAt;
    private String type;
    private String subject;
    private String description;
    private String priority;
    private String status;
    private String submitter;
    private String assignee;
    private String organization;
    private Iterable<String> tags;
    private Boolean hasIncidents;
    private String dueAt;
    private String via;

    public static TicketBuilder builder() {
        return new TicketBuilder();
    }

    public TicketBuilder setId(String id) {
        this.id = id;
        return this;
    }

    public TicketBuilder setUrl(String url) {
        this.url = url;
        return this;
    }

    public TicketBuilder setExternalId(String externalId) {
        this.externalId = externalId;
        return this;
    }

    public TicketBuilder setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public TicketBuilder setType(String type) {
        this.type = type;
        return this;
    }

    public TicketBuilder setSubject(String subject) {
        this.subject = subject;
        return this;
    }

    public TicketBuilder setDescription(String description) {
        this.description = description;
        return this;
    }

    public TicketBuilder setPriority(String priority) {
        this.priority = priority;
        return this;
    }

    public TicketBuilder setStatus(String status) {
        this.status = status;
        return this;
    }

    public TicketBuilder setSubmitter(String submitter) {
        this.submitter = submitter;
        return this;
    }

    public TicketBuilder setAssignee(String assignee) {
        this.assignee = assignee;
        return this;
    }

    public TicketBuilder setOrganization(String organization) {
        this.organization = organization;
        return this;
    }

    public TicketBuilder setTags(Iterable<String> tags) {
        this.tags = tags;
        return this;
    }

    public TicketBuilder setHasIncidents(Boolean hasIncidents) {
        this.hasIncidents = hasIncidents;
        return this;
    }

    public TicketBuilder setDueAt(String dueAt) {
        this.dueAt = dueAt;
        return this;
    }

    public TicketBuilder setVia(String via) {
        this.via = via;
        return this;
    }

    public Ticket create() {
        return new Ticket(id, url, externalId, createdAt, type, subject, description, priority, status, submitter, assignee, organization, tags, hasIncidents, dueAt, via);
    }
}
