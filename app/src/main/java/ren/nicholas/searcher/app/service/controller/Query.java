package ren.nicholas.searcher.app.service.controller;

import ren.nicholas.searcher.app.service.model.EntityType;

public class Query<T> {
    private final Class<T> resultType;
    private final EntityType entityType;
    private final String field;
    private final String term;

    public Query(final EntityType entityType, final String field, final String term) {
        this.entityType = entityType;
        this.field = field;
        this.term = term;
        this.resultType = entityType.type();
    }

    public String getField() {
        return field;
    }

    public String getTerm() {
        return term;
    }

    public EntityType getEntityType() {
        return entityType;
    }

    public Class<T> type() {
        return resultType;
    }
}
