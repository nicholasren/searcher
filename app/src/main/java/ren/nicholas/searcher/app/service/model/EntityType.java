package ren.nicholas.searcher.app.service.model;

public enum EntityType {
    USER() {
        public Class<User> type() {
            return User.class;
        }
    },
    ORGANIZATION {
        @Override
        public Class<Organization> type() {
            return Organization.class;
        }
    },
    TICKET {
        @Override
        public Class<Ticket> type() {
            return Ticket.class;
        }
    };

    public abstract <T> Class<T> type();
}
