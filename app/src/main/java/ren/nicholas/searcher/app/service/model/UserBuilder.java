package ren.nicholas.searcher.app.service.model;

import io.vavr.collection.List;
import io.vavr.collection.Seq;

public class UserBuilder {
    private Long id;
    private String url;
    private String externalId;
    private String name;
    private String alias;
    private String createdAt;
    private Boolean active;
    private Boolean verified;
    private Boolean shared;
    private String locale;
    private String timezone;
    private String lastLoginAt;
    private String email;
    private String phone;
    private String signature;
    private String organization;
    private Seq<String> tags;
    private Boolean suspended;
    private String role;
    private List<String> tickets;

    private UserBuilder() {
    }

    public static UserBuilder builder() {
        return new UserBuilder();
    }

    public UserBuilder setId(Long id) {
        this.id = id;
        return this;
    }

    public UserBuilder setUrl(String url) {
        this.url = url;
        return this;
    }

    public UserBuilder setExternalId(String externalId) {
        this.externalId = externalId;
        return this;
    }

    public UserBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public UserBuilder setAlias(String alias) {
        this.alias = alias;
        return this;
    }

    public UserBuilder setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public UserBuilder setActive(Boolean active) {
        this.active = active;
        return this;
    }

    public UserBuilder setVerified(Boolean verified) {
        this.verified = verified;
        return this;
    }

    public UserBuilder setShared(Boolean shared) {
        this.shared = shared;
        return this;
    }

    public UserBuilder setLocale(String locale) {
        this.locale = locale;
        return this;
    }

    public UserBuilder setTimezone(String timezone) {
        this.timezone = timezone;
        return this;
    }

    public UserBuilder setLastLoginAt(String lastLoginAt) {
        this.lastLoginAt = lastLoginAt;
        return this;
    }

    public UserBuilder setEmail(String email) {
        this.email = email;
        return this;
    }

    public UserBuilder setPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public UserBuilder setSignature(String signature) {
        this.signature = signature;
        return this;
    }

    public UserBuilder setTags(Seq<String> tags) {
        this.tags = tags;
        return this;
    }

    public UserBuilder setSuspended(Boolean suspended) {
        this.suspended = suspended;
        return this;
    }

    public UserBuilder setRole(String role) {
        this.role = role;
        return this;
    }

    public User create() {
        return new User(id, url, externalId, name, alias, createdAt, active, verified, shared, locale, timezone, lastLoginAt, email, phone, signature, organization, tags, suspended, role, tickets);
    }

    public UserBuilder setOrganization(String organization) {
        this.organization = organization;
        return this;
    }

    public UserBuilder setTickets(List<String> tickets) {
        this.tickets = tickets;
        return this;
    }
}