package ren.nicholas.searcher.app.service.extractor;

import ren.nicholas.searcher.app.service.model.Ticket;
import ren.nicholas.searcher.app.service.model.TicketBuilder;
import ren.nicholas.searcher.search.api.Document;

class TicketExtractor implements Extractor<Ticket> {

    private final AssociationExtractor associationExtractor;

    TicketExtractor(final AssociationExtractor associationExtractor) {
        this.associationExtractor = associationExtractor;
    }

    @Override
    public Ticket apply(final Document doc) {
        return TicketBuilder.builder()
                .setId(doc.stringValueOf("_id"))
                .setUrl(doc.stringValueOf("url"))
                .setExternalId(doc.stringValueOf("external_id"))
                .setCreatedAt(doc.stringValueOf("created_at"))
                .setType(doc.stringValueOf("type"))
                .setSubject(doc.stringValueOf("subject"))
                .setDescription(doc.stringValueOf("description"))
                .setPriority(doc.stringValueOf("priority"))
                .setStatus(doc.stringValueOf("status"))
                .setSubmitter(associationExtractor.userBy(doc.numericValueOf("submitter_id")))
                .setAssignee(associationExtractor.userBy(doc.numericValueOf("assignee_id")))
                .setOrganization(associationExtractor.organizationBy(doc.numericValueOf("organization_id")))
                .setTags(doc.collectionValueOf("tags"))
                .setHasIncidents(doc.booleanValueOf("has_incidents"))
                .setDueAt(doc.stringValueOf("due_at"))
                .setVia(doc.stringValueOf("via"))
                .create();
    }
}
