package ren.nicholas.searcher.app.cli;

import io.vavr.control.Try;
import org.beryx.textio.TextIO;
import org.beryx.textio.TextIoFactory;
import ren.nicholas.searcher.app.service.SearchService;

import static ren.nicholas.searcher.app.service.SearchServiceLauncher.service;


public final class App {

    public static void main(final String[] args) {
        IO io = io();

        workingDir(args)
                .flatMap(App::lunchService)
                .flatMap(app -> new Flow(app, io).tryRun())
                .onFailure(io::show);
    }

    private static IO io() {
        TextIO textIO = TextIoFactory.getTextIO();
        return new IO(textIO);
    }

    private static Try<String> workingDir(final String[] args) {
        return Try.of(() -> args[0]);
    }

    private static Try<SearchService> lunchService(final String documentsDir) {
        return service()
                .workingDir(documentsDir)
                .launch();
    }

    private App() {
    }
}
