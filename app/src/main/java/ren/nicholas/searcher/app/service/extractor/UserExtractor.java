package ren.nicholas.searcher.app.service.extractor;

import ren.nicholas.searcher.app.service.model.User;
import ren.nicholas.searcher.app.service.model.UserBuilder;
import ren.nicholas.searcher.search.api.Document;

class UserExtractor implements Extractor<User> {

    private final AssociationExtractor associationExtractor;

    UserExtractor(final AssociationExtractor associationExtractor) {
        this.associationExtractor = associationExtractor;
    }

    @Override
    public User apply(final Document document) {
        Long id = document.numericValueOf("_id");
        return UserBuilder.builder()
                .setId(id)
                .setUrl(document.stringValueOf("url"))
                .setExternalId(document.stringValueOf("external_id"))
                .setName(document.stringValueOf("name"))
                .setAlias(document.stringValueOf("alias"))
                .setCreatedAt(document.stringValueOf("created_at"))
                .setActive(document.booleanValueOf("active"))
                .setVerified(document.booleanValueOf("verified"))
                .setShared(document.booleanValueOf("shared"))
                .setLocale(document.stringValueOf("locale"))
                .setTimezone(document.stringValueOf("timezone"))
                .setLastLoginAt(document.stringValueOf("last_login_at"))
                .setEmail(document.stringValueOf("email"))
                .setPhone(document.stringValueOf("phone"))
                .setSignature(document.stringValueOf("signature"))
                .setOrganization(associationExtractor.organizationBy(document.numericValueOf("organization_id")))
                .setTags(document.collectionValueOf("tags"))
                .setSuspended(document.booleanValueOf("suspended"))
                .setRole(document.stringValueOf("role"))
                .setTickets(associationExtractor.ticketsOf(id))
                .create();
    }
}
