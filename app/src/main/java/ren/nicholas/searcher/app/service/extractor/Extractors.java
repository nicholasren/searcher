package ren.nicholas.searcher.app.service.extractor;

import io.vavr.collection.HashMap;
import io.vavr.collection.Map;
import ren.nicholas.searcher.app.service.model.Organization;
import ren.nicholas.searcher.app.service.model.Ticket;
import ren.nicholas.searcher.app.service.model.User;


public class Extractors {

    private final Map<Class, Extractor> extractors;

    public Extractors(final AssociationExtractor associationExtractor) {
        this.extractors = HashMap.of(
                User.class, new UserExtractor(associationExtractor),
                Organization.class, new OrganizationExtractor(),
                Ticket.class, new TicketExtractor(associationExtractor)
        );
    }

    public <T> Extractor<T> of(final Class<T> t) {
        return (Extractor<T>) extractors.get(t).get();
    }
}
