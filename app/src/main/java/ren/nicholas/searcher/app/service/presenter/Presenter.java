package ren.nicholas.searcher.app.service.presenter;


import io.vavr.collection.List;
import ren.nicholas.searcher.app.service.model.Schemas;
import ren.nicholas.searcher.app.util.Reflections;

import java.util.Objects;
import java.util.function.Function;

import static com.google.common.base.Joiner.on;
import static java.lang.String.format;
import static java.lang.System.lineSeparator;

public class Presenter<T> implements Function<T, String> {
    private static final int NAME_COLUMN_WIDTH = 20;
    private static final int VALUE_COLUMN_WIDTH = 149;
    private static final String HEADER = "+" + "-".repeat(NAME_COLUMN_WIDTH + VALUE_COLUMN_WIDTH + 1) + "+";
    private static final String FIELD_ROW_FORMAT = "|%-" + NAME_COLUMN_WIDTH + "s|%-" + VALUE_COLUMN_WIDTH + "s|";

    @SuppressWarnings("checkstyle")
    public static <T> Presenter<T> of(final Class<T> type) {
        return new Presenter<>();
    }

    @Override
    public String apply(final T object) {
        List<String> lines = Reflections.valuesOf(object, Schemas.fieldsOf(object))
                .map(line -> format(FIELD_ROW_FORMAT, line._1, asString(line._2)));


        return on(lineSeparator()).join(lines.prepend(HEADER).append(HEADER));
    }

    public String show(final Iterable<String> schema) {
        return on(lineSeparator()).join(schema);
    }


    private static String asString(final Object obj) {
        if (obj instanceof Iterable) {
            return "[" + on(", ").join((Iterable<?>) obj) + "]";
        } else {
            return Objects.toString(obj);
        }
    }

}
