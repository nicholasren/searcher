package ren.nicholas.searcher.app.util;

import io.vavr.Tuple;
import io.vavr.Tuple2;
import io.vavr.collection.List;
import io.vavr.control.Try;

import static com.google.common.base.CaseFormat.LOWER_UNDERSCORE;
import static com.google.common.base.CaseFormat.UPPER_CAMEL;

public final class Reflections {

    public static <T> List<Tuple2<String, Object>> valuesOf(final T object, final List<String> fields) {
        return fields.map(field -> valueOf(object, field));
    }

    private static <T> Tuple2<String, Object> valueOf(final T object, final String fieldName) {
        return Try.of(() -> {
            var name = getterNameFor(fieldName);
            var getter = object.getClass().getMethod(name);
            return Tuple.of(fieldName, getter.invoke(object));
        }).get();
    }

    private static String getterNameFor(final String fieldName) {
        return "get" + LOWER_UNDERSCORE.to(UPPER_CAMEL, fieldName);
    }

    private Reflections() {
    }
}
