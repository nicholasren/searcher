package ren.nicholas.searcher.app.service.model;

public class Ticket {
    private final String id;
    private final String url;
    private final String externalId;
    private final String createdAt;
    private final String type;
    private final String subject;
    private final String description;
    private final String priority;
    private final String status;
    private final String submitter;
    private final String assignee;
    private final String organization;
    private final Iterable<String> tags;
    private final Boolean hasIncidents;
    private final String dueAt;
    private final String via;

    Ticket(String id, String url, String externalId, String createdAt, String type, String subject, String description, String priority, String status, String submitter, String assignee, String organization, Iterable<String> tags, Boolean hasIncidents, String dueAt, String via) {
        this.id = id;
        this.url = url;
        this.externalId = externalId;
        this.createdAt = createdAt;
        this.type = type;
        this.subject = subject;
        this.description = description;
        this.priority = priority;
        this.status = status;
        this.submitter = submitter;
        this.assignee = assignee;
        this.organization = organization;
        this.tags = tags;
        this.hasIncidents = hasIncidents;
        this.dueAt = dueAt;
        this.via = via;
    }

    public String getId() {
        return id;
    }

    public String getUrl() {
        return url;
    }

    public String getExternalId() {
        return externalId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getType() {
        return type;
    }

    public String getSubject() {
        return subject;
    }

    public String getDescription() {
        return description;
    }

    public String getPriority() {
        return priority;
    }

    public String getStatus() {
        return status;
    }

    public String getSubmitter() {
        return submitter;
    }

    public String getAssignee() {
        return assignee;
    }

    public String getOrganization() {
        return organization;
    }

    public Iterable<String> getTags() {
        return tags;
    }

    public Boolean getHasIncidents() {
        return hasIncidents;
    }

    public String getDueAt() {
        return dueAt;
    }

    public String getVia() {
        return via;
    }
}

