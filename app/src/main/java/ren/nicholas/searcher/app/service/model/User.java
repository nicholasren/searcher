package ren.nicholas.searcher.app.service.model;

import io.vavr.collection.List;
import io.vavr.collection.Seq;

public class User {
    private final Long id;
    private final String url;
    private final String externalId;
    private final String name;
    private final String alias;
    private final String createdAt;
    private final Boolean active;
    private final Boolean verified;
    private final Boolean shared;
    private final String locale;
    private final String timezone;
    private final String lastLoginAt;
    private final String email;
    private final String phone;
    private final String signature;
    private final String organization;
    private final Iterable<String> tags;
    private final Boolean suspended;
    private final String role;
    private List<String> tickets;

    public User(Long id, String url, String externalId, String name, String alias, String createdAt, Boolean active, Boolean verified, Boolean shared, String locale, String timezone, String lastLoginAt, String email, String phone, String signature, String organization, Seq<String> tags, Boolean suspended, String role, List<String> tickets) {
        this.id = id;
        this.url = url;
        this.externalId = externalId;
        this.name = name;
        this.alias = alias;
        this.createdAt = createdAt;
        this.active = active;
        this.verified = verified;
        this.shared = shared;
        this.locale = locale;
        this.timezone = timezone;
        this.lastLoginAt = lastLoginAt;
        this.email = email;
        this.phone = phone;
        this.signature = signature;
        this.organization = organization;
        this.tags = tags;
        this.suspended = suspended;
        this.role = role;
        this.tickets = tickets;
    }

    public Long getId() {
        return id;
    }

    public String getUrl() {
        return url;
    }

    public String getExternalId() {
        return externalId;
    }

    public String getName() {
        return name;
    }

    public String getAlias() {
        return alias;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public Boolean getActive() {
        return active;
    }

    public Boolean getVerified() {
        return verified;
    }

    public Boolean getShared() {
        return shared;
    }

    public String getLocale() {
        return locale;
    }

    public String getTimezone() {
        return timezone;
    }

    public String getLastLoginAt() {
        return lastLoginAt;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getSignature() {
        return signature;
    }

    public String getOrganization() {
        return organization;
    }

    public Iterable<String> getTags() {
        return tags;
    }

    public Boolean getSuspended() {
        return suspended;
    }

    public String getRole() {
        return role;
    }

    public List<String> getTickets() {
        return tickets;
    }
}
