package ren.nicholas.searcher.app.cli;

import io.vavr.control.Try;
import ren.nicholas.searcher.app.service.SearchService;


public class Flow {

    private final SearchService searchService;
    private final IO io;

    public Flow(final SearchService searchService, final IO io) {
        this.searchService = searchService;
        this.io = io;
    }

    public Try<Void> tryRun() {
        return Try.run(this::run);
    }

    private void run() {
        io.welcome();
        do {
            Action action = io.askForAction();
            if (Action.SEARCH.equals(action)) {
                var query = io.query();
                io.showQuery(query);
                var results = searchService.by(query);
                if (results.isLeft()) {
                    io.show(results.getLeft());
                } else {
                    io.showResults(results.get());
                }
            } else if (Action.SCHEMA.equals(action)) {
                var entityType = io.askForEntityType();

                String schema = searchService.schemaFor(entityType);
                io.show(schema);
            }
        } while (io.askForContinue());

        io.dispose();
    }
}
