package ren.nicholas.searcher.app.service.extractor;

import io.vavr.collection.List;
import ren.nicholas.searcher.app.service.model.EntityType;
import ren.nicholas.searcher.search.api.Query;
import ren.nicholas.searcher.search.api.SearchEngine;

public class AssociationExtractor {

    private final SearchEngine searchEngine;

    public AssociationExtractor(final SearchEngine searchEngine) {
        this.searchEngine = searchEngine;
    }

    public String organizationBy(final Long id) {
        return nameOf(EntityType.ORGANIZATION.name(), String.valueOf(id));
    }

    public List<String> ticketsOf(final Long userId) {
        return searchEngine.search(bySubmitterId(userId)).map(doc -> doc.stringValueOf("subject"));
    }

    public String userBy(final Long id) {
        return nameOf(EntityType.USER.name(), String.valueOf(id));
    }

    private String nameOf(final String entityType, final String id) {
        return searchEngine
                .search(byId(entityType, id))
                .map(doc -> doc.stringValueOf("name"))
                .headOption()
                .getOrElse(defaultName(entityType, id));
    }

    private Query bySubmitterId(final Long userId) {
        return by(EntityType.TICKET.name(), "submitter_id", String.valueOf(userId));
    }

    private Query byId(final String entityType, final String id) {
        return new Query(entityType, "_id", id);
    }

    private String defaultName(final String entityType, final String id) {
        return String.format("Unknown %s id=%s", entityType, id);
    }

    private Query by(final String entityType, final String field, final String value) {
        return new Query(entityType, field, value);
    }
}
