package ren.nicholas.searcher.app.service.model;

public class OrganizationBuilder {
    private Long id;
    private String name;
    private String url;
    private String externalId;
    private Iterable<String> domainNames;
    private String createdAt;
    private String details;
    private Boolean sharedTickets;
    private Iterable<String> tags;

    public static OrganizationBuilder builder() {
        return new OrganizationBuilder();
    }

    public OrganizationBuilder setId(Long id) {
        this.id = id;
        return this;
    }

    public OrganizationBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public OrganizationBuilder setUrl(String url) {
        this.url = url;
        return this;
    }

    public OrganizationBuilder setExternalId(String externalId) {
        this.externalId = externalId;
        return this;
    }

    public OrganizationBuilder setDomainNames(Iterable<String> domainNames) {
        this.domainNames = domainNames;
        return this;
    }

    public OrganizationBuilder setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public OrganizationBuilder setDetails(String details) {
        this.details = details;
        return this;
    }

    public OrganizationBuilder setSharedTickets(Boolean sharedTickets) {
        this.sharedTickets = sharedTickets;
        return this;
    }

    public OrganizationBuilder setTags(Iterable<String> tags) {
        this.tags = tags;
        return this;
    }

    public Organization create() {
        return new Organization(id, url, externalId, name, domainNames, createdAt, details, sharedTickets, tags);
    }
}