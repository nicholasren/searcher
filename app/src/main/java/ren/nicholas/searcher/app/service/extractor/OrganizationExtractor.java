package ren.nicholas.searcher.app.service.extractor;

import ren.nicholas.searcher.app.service.model.Organization;
import ren.nicholas.searcher.search.api.Document;

import static ren.nicholas.searcher.app.service.model.OrganizationBuilder.builder;

class OrganizationExtractor implements Extractor<Organization> {
    @Override
    public Organization apply(final Document doc) {
        return builder().setId(doc.numericValueOf("_id"))
                .setUrl(doc.stringValueOf("url"))
                .setExternalId(doc.stringValueOf("external_id"))
                .setName(doc.stringValueOf("name"))
                .setDomainNames(doc.collectionValueOf("domain_names"))
                .setCreatedAt(doc.stringValueOf("created_at"))
                .setDetails(doc.stringValueOf("details"))
                .setSharedTickets(doc.booleanValueOf("shared_tickets"))
                .setTags(doc.collectionValueOf("tags"))
                .create();
    }
}
