package ren.nicholas.searcher.app.service;

import io.vavr.collection.HashMap;
import io.vavr.control.Try;
import ren.nicholas.searcher.app.service.controller.SchemaController;
import ren.nicholas.searcher.app.service.controller.SearchController;
import ren.nicholas.searcher.app.service.extractor.AssociationExtractor;
import ren.nicholas.searcher.app.service.extractor.Extractors;
import ren.nicholas.searcher.app.service.model.EntityType;
import ren.nicholas.searcher.search.impl.engine.SearchEngineLauncher;

public class SearchServiceLauncher {
    private String workingDir;

    public static SearchServiceLauncher service() {
        return new SearchServiceLauncher();
    }


    public Try<SearchService> launch() {
        return SearchEngineLauncher
                .launch(indexConfigs())
                .map(searchEngine -> {
                    var associationExtractor = new AssociationExtractor(searchEngine);
                    var extractors = new Extractors(associationExtractor);
                    var searchController = new SearchController(searchEngine, extractors);
                    var schemaController = new SchemaController();

                    return new SearchService(searchController, schemaController);
                });
    }

    public SearchServiceLauncher workingDir(final String workingDir) {
        this.workingDir = workingDir;
        return this;
    }

    private HashMap<String, String> indexConfigs() {
        return HashMap.of(
                EntityType.USER.name(), workingDir + "/users.json",
                EntityType.TICKET.name(), workingDir + "/tickets.json",
                EntityType.ORGANIZATION.name(), workingDir + "/organizations.json"
        );
    }
}

