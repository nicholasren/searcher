package ren.nicholas.searcher.app.service.model;

public class Organization {
    private final Long id;
    private final String url;
    private final String externalId;
    private final String name;
    private final Iterable<String> domainNames;
    private final String createdAt;
    private final String details;
    private final Boolean sharedTickets;
    private final Iterable<String> tags;

    public Organization(Long id, String url, String externalId, String name, Iterable<String> domainNames, String createdAt, String details, Boolean sharedTickets, Iterable<String> tags) {
        this.id = id;
        this.url = url;
        this.externalId = externalId;
        this.name = name;
        this.domainNames = domainNames;
        this.createdAt = createdAt;
        this.details = details;
        this.sharedTickets = sharedTickets;
        this.tags = tags;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }

    public String getExternalId() {
        return externalId;
    }

    public Iterable<String> getDomainNames() {
        return domainNames;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getDetails() {
        return details;
    }

    public Boolean getSharedTickets() {
        return sharedTickets;
    }

    public Iterable<String> getTags() {
        return tags;
    }
}
