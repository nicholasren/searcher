package ren.nicholas.searcher.app.service.controller;

import io.vavr.collection.List;
import io.vavr.control.Either;
import ren.nicholas.searcher.app.service.extractor.Extractor;
import ren.nicholas.searcher.app.service.extractor.Extractors;
import ren.nicholas.searcher.app.service.presenter.Presenter;
import ren.nicholas.searcher.search.api.SearchEngine;

import static ren.nicholas.searcher.app.service.controller.QueryConverter.from;

public class SearchController {

    private final Extractors extractors;
    private final SearchEngine searcher;

    public SearchController(final SearchEngine searcher, final Extractors extractors) {
        this.extractors = extractors;
        this.searcher = searcher;
    }

    public <T> Either<String, List<String>> by(final Query<T> query) {
        Extractor<T> extractor = extractors.of(query.type());
        Presenter<T> presenter = Presenter.of(query.type());

        Either<String, ren.nicholas.searcher.search.api.Query> convertedQuery = from(query);

        return convertedQuery
                .map(q -> searcher.search(q)
                        .map(extractor)
                        .map(presenter));
    }
}
