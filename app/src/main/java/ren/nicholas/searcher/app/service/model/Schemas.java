package ren.nicholas.searcher.app.service.model;

import io.vavr.collection.HashMap;
import io.vavr.collection.List;
import io.vavr.collection.Map;

public final class Schemas {

    private static final Map<Class, List<String>> PRINTABLE_FIELDS_FOR_CLASS = HashMap.of(
            User.class, List.of(
                    "_id",
                    "url",
                    "external_id",
                    "name",
                    "alias",
                    "created_at",
                    "active",
                    "verified",
                    "shared",
                    "locale",
                    "timezone",
                    "last_login_at",
                    "email",
                    "phone",
                    "signature",
                    "organization",
                    "tags",
                    "suspended",
                    "role",
                    "tickets"
            ),

            Organization.class, List.of(
                    "_id",
                    "url",
                    "external_id",
                    "name",
                    "domain_names",
                    "created_at",
                    "details",
                    "shared_tickets",
                    "tags"
            ),
            Ticket.class, List.of(
                    "_id",
                    "url",
                    "external_id",
                    "created_at",
                    "type",
                    "subject",
                    "description",
                    "priority",
                    "status",
                    "submitter",
                    "assignee",
                    "organization",
                    "tags",
                    "has_incidents",
                    "due_at",
                    "via"
            )
    );

    public static <T> List<String> fieldsOf(final Class<T> clazz) {
        return PRINTABLE_FIELDS_FOR_CLASS.get(clazz).get();
    }

    public static <T> List<String> fieldsOf(final T object) {
        return fieldsOf(object.getClass());
    }

    private Schemas() {
    }
}
