package ren.nicholas.searcher.app.service.extractor;

import io.vavr.collection.List;
import io.vavr.control.Either;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import ren.nicholas.searcher.app.service.controller.Query;
import ren.nicholas.searcher.app.service.model.EntityType;
import ren.nicholas.searcher.app.service.model.User;
import ren.nicholas.searcher.search.impl.utils.Fixture;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.lenient;
import static ren.nicholas.searcher.app.service.controller.QueryConverter.from;

@ExtendWith(MockitoExtension.class)
class UserExtractorTest {

    @Mock
    private AssociationExtractor associationExtractor;

    private UserExtractor extractor;

    @BeforeEach
    void setup() {
        lenient().when(associationExtractor.organizationBy(any())).thenReturn("BIG CORP 1");
        lenient().when(associationExtractor.ticketsOf(any())).thenReturn(List.of("ticket1", "ticket2"));

        extractor = new UserExtractor(associationExtractor);
    }

    @Nested
    class Extraction {
        @Test
        void should_extract_user_attribute_values_from_matched_document() {
            User user = extractor.apply(Fixture.DocumentData.DOCUMENT_USER);

            assertThat(user.getId()).isEqualTo(Fixture.USER_VALUES.get("_id").get());
            assertThat(user.getUrl()).isEqualTo(Fixture.USER_VALUES.get("url").get());
            assertThat(user.getExternalId()).isEqualTo(Fixture.USER_VALUES.get("external_id").get());
            assertThat(user.getName()).isEqualTo(Fixture.USER_VALUES.get("name").get());
            assertThat(user.getAlias()).isEqualTo(Fixture.USER_VALUES.get("alias").get());
            assertThat(user.getCreatedAt()).isEqualTo(Fixture.USER_VALUES.get("created_at").get());
            assertThat(user.getActive()).isEqualTo(Fixture.USER_VALUES.get("active").get());
            assertThat(user.getVerified()).isEqualTo(Fixture.USER_VALUES.get("verified").get());
            assertThat(user.getShared()).isEqualTo(Fixture.USER_VALUES.get("shared").get());
            assertThat(user.getLocale()).isEqualTo(Fixture.USER_VALUES.get("locale").get());
            assertThat(user.getTimezone()).isEqualTo(Fixture.USER_VALUES.get("timezone").get());
            assertThat(user.getLastLoginAt()).isEqualTo(Fixture.USER_VALUES.get("last_login_at").get());
            assertThat(user.getEmail()).isEqualTo(Fixture.USER_VALUES.get("email").get());
            assertThat(user.getPhone()).isEqualTo(Fixture.USER_VALUES.get("phone").get());
            assertThat(user.getSignature()).isEqualTo(Fixture.USER_VALUES.get("signature").get());
            assertThat(user.getSuspended()).isEqualTo(Fixture.USER_VALUES.get("suspended").get());
            assertThat(user.getTags()).isEqualTo(Fixture.USER_VALUES.get("tags").get());
            assertThat(user.getRole()).isEqualTo(Fixture.USER_VALUES.get("role").get());
        }

        @Nested
        class OrganizationRetrieving {
            @Test
            void should_populate_user_organization_from_matched_document() {
                User user = extractor.apply(Fixture.DocumentData.DOCUMENT_USER);

                assertThat(user.getOrganization()).isEqualTo("BIG CORP 1");
            }
        }

        @Nested
        class TicketRetrieving {
            @Test
            void should_populate_user_tickets_from_matched_document() {
                User user = extractor.apply(Fixture.DocumentData.DOCUMENT_USER);

                assertThat(user.getTickets()).contains("ticket1", "ticket2");
            }
        }
    }

    @Nested
    class QueryConversion {

        @ParameterizedTest
        @ValueSource(strings = {"_id", "url", "external_id", "name", "alias", "created_at", "active", "verified", "shared", "locale", "timezone", "last_login_at", "email", "phone", "signature", "organization", "tags", "suspended", "role"})
        void should_return_converted_query_for_valid_query_fields(String field) {
            var query = new Query(EntityType.USER, field, "value");

            Either<String, ren.nicholas.searcher.search.api.Query> converted = from(query);

            assertThat(converted.isRight()).isTrue();
            assertThat(converted.get().getEntityType()).isEqualTo("USER");
            assertThat(converted.get().getName()).isEqualTo(field);
            assertThat(converted.get().getValue()).isEqualTo("value");
        }

        @Test
        void should_return_error_message_for_invalid_query_field() {
            var query = new Query(EntityType.USER, "invalid_field", "value");

            Either<String, ren.nicholas.searcher.search.api.Query> converted = from(query);

            assertThat(converted.isLeft()).isTrue();
            assertThat(converted.getLeft()).isEqualTo("Invalid field invalid_field");
        }
    }
}
