package ren.nicholas.searcher.app.cli;

import io.vavr.collection.List;
import org.beryx.textio.TextIO;
import org.beryx.textio.mock.MockTextTerminal;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ren.nicholas.searcher.app.service.controller.Query;
import ren.nicholas.searcher.app.service.model.EntityType;

import static org.assertj.core.api.Assertions.assertThat;

class IOTest {

    private MockTextTerminal terminal;
    private IO io;

    @BeforeEach
    void before() {
        terminal = new MockTextTerminal();
        io = new IO(new TextIO(terminal));
    }

    @Test
    void should_print_welcome_message() {
        io.welcome();

        assertThat(terminal.getOutput()).contains(
                "###############################",
                "#   Welcome to Zendesk search #",
                "###############################"
        );
    }

    @Test
    void should_prompt_with_available_actions() {
        terminal.getInputs()
                .addAll(List.of(
                        String.valueOf(Action.SEARCH.ordinal() + 1)
                ).asJava());

        io.askForAction();

        assertThat(terminal.getOutput()).contains(
                "Please select a action:",
                "* 1: SEARCH",
                "2: SCHEMA"
        );
    }

    @Test
    void should_prompt_with_searchable_entities() {
        terminal.getInputs().addAll(queryInputs());

        io.query();

        assertThat(terminal.getOutput()).contains(
                "Please tell us what you want to search:",
                "* 1: USER",
                "2: ORGANIZATION",
                "3: TICKET",
                "Enter search field",
                "Enter search term, (Press 'Enter' to search for empty value)"
        );
    }


    @Test
    void should_print_searching_criteria() {
        terminal.getInputs().addAll(queryInputs());
        Query query = io.query();

        io.showQuery(query);

        assertThat(terminal.getOutput()).contains("Searching USER for name with a value of jay");
    }

    private java.util.List<String> queryInputs() {
        return List.of(
                String.valueOf(EntityType.USER.ordinal() + 1),
                "name",
                "jay"
        ).asJava();
    }

    @Test
    void should_display_result_when_result_found() {
        io.showResults(List.of("Result 1", "Result 2"));

        assertThat(terminal.getOutput()).contains("Result 1", "Result 2");
    }

    @Test
    void should_display_no_result_message_when_no_result_found() {

        io.showResults(List.empty());

        assertThat(terminal.getOutput()).contains("No result found");
    }

    @Test
    void should_prompt_for_following_action() {
        terminal.getInputs().add("Y");

        Boolean proceed = io.askForContinue();

        assertThat(proceed).isTrue();
        assertThat(terminal.getOutput()).contains("Continue?");
    }

    @Test
    void should_show_error_message(){
        io.show(new NullPointerException("error"));

        assertThat(terminal.getOutput()).contains("Something Bad Happened, Press 'Ctrl + C' to quit");
    }

    @Test
    void should_show_message(){
        io.show("Hello");
        assertThat(terminal.getOutput()).contains("Hello");
    }
}