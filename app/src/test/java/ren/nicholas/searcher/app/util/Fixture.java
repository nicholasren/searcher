package ren.nicholas.searcher.app.util;

import io.vavr.collection.List;
import ren.nicholas.searcher.app.service.model.Organization;
import ren.nicholas.searcher.app.service.model.OrganizationBuilder;
import ren.nicholas.searcher.app.service.model.User;
import ren.nicholas.searcher.app.service.model.UserBuilder;

import static java.lang.System.lineSeparator;

public class Fixture {

    public static class DomainData {
        public static final User DOMAIN_USER = UserBuilder.builder()
                .setId(1L)
                .setUrl("http://initech.zendesk.com/api/v2/users/1.json")
                .setExternalId("74341f74-9c79-49d5-9611-87ef9b6eb75f")
                .setName("Francisca Rasmussen")
                .setAlias("Miss Coffey")
                .setCreatedAt("2016-04-15T05:19:46 -10:00")
                .setActive(true)
                .setVerified(true)
                .setShared(false)
                .setLocale("en-AU")
                .setTimezone("Sri Lanka")
                .setLastLoginAt("2013-08-04T01:03:27 -10:00")
                .setEmail("coffeyrasmussen@flotonic.com")
                .setPhone("8335-422-718")
                .setSignature("Don't Worry Be Happy!")
                .setOrganization("Multron")
                .setTags(List.of("Springville",
                        "Sutton",
                        "Hartsville/Hartley",
                        "Diaperville"))
                .setSuspended(true)
                .setRole("admin")
                .setTickets(List.of("A Nuisance in Kiribati", "A Nuisance in Saint Lucia"))
                .create();

        public static final Organization DOMAIN_ORGANIZATION = OrganizationBuilder.builder()
                .setId(101L)
                .setUrl("http://initech.zendesk.com/api/v2/organizations/101.json")
                .setExternalId("9270ed79-35eb-4a38-a46f-35725197ea8d")
                .setName("Enthaze")
                .setDomainNames(List.of("kage.com",
                        "ecratic.com",
                        "endipin.com",
                        "zentix.com"))
                .setCreatedAt("2016-05-21T11:10:28 -10:00")
                .setDetails("MegaCorp")
                .setSharedTickets(false)
                .setTags(List.of( "Fulton",
                        "West",
                        "Rodriguez",
                        "Farley"))
                .create();


        public static final String PRESENTABLE_USER_SCHEMA =
                "_id" + lineSeparator() +
                "url" + lineSeparator() +
                "external_id" + lineSeparator() +
                "name" + lineSeparator() +
                "alias" + lineSeparator() +
                "created_at" + lineSeparator() +
                "active" + lineSeparator() +
                "verified" + lineSeparator() +
                "shared" + lineSeparator() +
                "locale" + lineSeparator() +
                "timezone" + lineSeparator() +
                "last_login_at" + lineSeparator() +
                "email" + lineSeparator() +
                "phone" + lineSeparator() +
                "signature" + lineSeparator() +
                "organization" + lineSeparator() +
                "tags" + lineSeparator() +
                "suspended" + lineSeparator() +
                "role" + lineSeparator() +
                "tickets";

        public static final String PRESENTABLE_USER =
                "+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+" + lineSeparator() +
                "|_id                 |1                                                                                                                                                    |" + lineSeparator() +
                "|url                 |http://initech.zendesk.com/api/v2/users/1.json                                                                                                       |" + lineSeparator() +
                "|external_id         |74341f74-9c79-49d5-9611-87ef9b6eb75f                                                                                                                 |" + lineSeparator() +
                "|name                |Francisca Rasmussen                                                                                                                                  |" + lineSeparator() +
                "|alias               |Miss Coffey                                                                                                                                          |" + lineSeparator() +
                "|created_at          |2016-04-15T05:19:46 -10:00                                                                                                                           |" + lineSeparator() +
                "|active              |true                                                                                                                                                 |" + lineSeparator() +
                "|verified            |true                                                                                                                                                 |" + lineSeparator() +
                "|shared              |false                                                                                                                                                |" + lineSeparator() +
                "|locale              |en-AU                                                                                                                                                |" + lineSeparator() +
                "|timezone            |Sri Lanka                                                                                                                                            |" + lineSeparator() +
                "|last_login_at       |2013-08-04T01:03:27 -10:00                                                                                                                           |" + lineSeparator() +
                "|email               |coffeyrasmussen@flotonic.com                                                                                                                         |" + lineSeparator() +
                "|phone               |8335-422-718                                                                                                                                         |" + lineSeparator() +
                "|signature           |Don't Worry Be Happy!                                                                                                                                |" + lineSeparator() +
                "|organization        |Multron                                                                                                                                              |" + lineSeparator() +
                "|tags                |[Springville, Sutton, Hartsville/Hartley, Diaperville]                                                                                               |" + lineSeparator() +
                "|suspended           |true                                                                                                                                                 |" + lineSeparator() +
                "|role                |admin                                                                                                                                                |" + lineSeparator() +
                "|tickets             |[A Nuisance in Kiribati, A Nuisance in Saint Lucia]                                                                                                  |" + lineSeparator() +
                "+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+";

        public static final String PRESENTABLE_ORGANIZATION =
                "+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+" + lineSeparator() +
                "|_id                 |101                                                                                                                                                  |" + lineSeparator() +
                "|url                 |http://initech.zendesk.com/api/v2/organizations/101.json                                                                                             |" + lineSeparator() +
                "|external_id         |9270ed79-35eb-4a38-a46f-35725197ea8d                                                                                                                 |" + lineSeparator() +
                "|name                |Enthaze                                                                                                                                              |" + lineSeparator() +
                "|domain_names        |[kage.com, ecratic.com, endipin.com, zentix.com]                                                                                                     |" + lineSeparator() +
                "|created_at          |2016-05-21T11:10:28 -10:00                                                                                                                           |" + lineSeparator() +
                "|details             |MegaCorp                                                                                                                                             |" + lineSeparator() +
                "|shared_tickets      |false                                                                                                                                                |" + lineSeparator() +
                "|tags                |[Fulton, West, Rodriguez, Farley]                                                                                                                    |" + lineSeparator() +
                "+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+" ;

        public static final String PRESENTABLE_TICKET =
                "+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+" + lineSeparator() +
                "|_id                 |436bf9b0-1147-4c0a-8439-6f79833bff5b                                                                                                                 |" + lineSeparator() +
                "|url                 |http://initech.zendesk.com/api/v2/tickets/436bf9b0-1147-4c0a-8439-6f79833bff5b.json                                                                  |" + lineSeparator() +
                "|external_id         |9210cdc9-4bee-485f-a078-35396cd74063                                                                                                                 |" + lineSeparator() +
                "|created_at          |2016-04-28T11:19:34 -10:00                                                                                                                           |" + lineSeparator() +
                "|type                |incident                                                                                                                                             |" + lineSeparator() +
                "|subject             |A Catastrophe in Korea (North)                                                                                                                       |" + lineSeparator() +
                "|description         |Nostrud ad sit velit cupidatat laboris ipsum nisi amet laboris ex exercitation amet et proident. Ipsum fugiat aute dolore tempor nostrud velit ipsum.|" + lineSeparator() +
                "|priority            |high                                                                                                                                                 |" + lineSeparator() +
                "|status              |pending                                                                                                                                              |" + lineSeparator() +
                "|submitter           |Elma Castro                                                                                                                                          |" + lineSeparator() +
                "|assignee            |Harris Côpeland                                                                                                                                      |" + lineSeparator() +
                "|organization        |Zentry                                                                                                                                               |" + lineSeparator() +
                "|tags                |[Ohio, Pennsylvania, American Samoa, Northern Mariana Islands]                                                                                       |" + lineSeparator() +
                "|has_incidents       |false                                                                                                                                                |" + lineSeparator() +
                "|due_at              |2016-07-31T02:37:50 -10:00                                                                                                                           |" + lineSeparator() +
                "|via                 |web                                                                                                                                                  |" + lineSeparator() +
                "+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+";
    }
}
