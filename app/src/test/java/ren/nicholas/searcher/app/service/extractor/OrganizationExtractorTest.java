package ren.nicholas.searcher.app.service.extractor;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import ren.nicholas.searcher.app.service.model.Organization;
import ren.nicholas.searcher.search.impl.utils.Fixture;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class OrganizationExtractorTest {

    private OrganizationExtractor extractor;

    @BeforeEach
    void setup() {
        extractor = new OrganizationExtractor();
    }

    @Test
    void should_extract_organization_attribute_values_from_matched_document() {
        Organization organization = extractor.apply(Fixture.DocumentData.DOCUMENT_ORGANIZATION);

        assertThat(organization.getId()).isEqualTo(Fixture.ORG_VALUES.get("_id").get());
        assertThat(organization.getUrl()).isEqualTo(Fixture.ORG_VALUES.get("url").get());
        assertThat(organization.getExternalId()).isEqualTo(Fixture.ORG_VALUES.get("external_id").get());
        assertThat(organization.getName()).isEqualTo(Fixture.ORG_VALUES.get("name").get());
        assertThat(organization.getDomainNames()).isEqualTo(Fixture.ORG_VALUES.get("domain_names").get());
        assertThat(organization.getCreatedAt()).isEqualTo(Fixture.ORG_VALUES.get("created_at").get());
        assertThat(organization.getDetails()).isEqualTo(Fixture.ORG_VALUES.get("details").get());
        assertThat(organization.getSharedTickets()).isEqualTo(Fixture.ORG_VALUES.get("shared_tickets").get());
        assertThat(organization.getTags()).isEqualTo(Fixture.ORG_VALUES.get("tags").get());
    }
}
