package ren.nicholas.searcher.app.cli;

import io.vavr.collection.List;
import io.vavr.control.Either;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import ren.nicholas.searcher.app.service.SearchService;
import ren.nicholas.searcher.app.service.controller.Query;
import ren.nicholas.searcher.app.service.model.EntityType;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class FlowTest {
    @Mock
    private SearchService searchService;
    @Mock
    private IO io;

    private List<String> searchResult = List.of("Result 1", "Result 2");
    private Flow flow;

    @BeforeEach
    void before() {
        flow = new Flow(searchService, io);
    }

    @Test
    void should_print_welcome_message() {
        flow.tryRun();
        verify(io, times(1)).welcome();
    }

    @Test
    void should_ask_for_users_action() {
        flow.tryRun();
        verify(io).askForAction();
    }

    @Nested
    class Search {

        @Mock
        private Query query;

        @BeforeEach
        void before() {
            when(io.askForAction()).thenReturn(Action.SEARCH);
            when(io.query()).thenReturn(query);
        }

        @Test
        void should_collect_query() {
            flow.tryRun();
            verify(io).query();
        }

        @Test
        void should_print_searching_criteria() {
            flow.tryRun();

            verify(io, times(1)).showQuery(query);
        }

        @Test
        void should_call_search_app_for_result_by_query() {
            flow.tryRun();

            verify(searchService, times(1)).by(query);
        }

        @Test
        void should_display_result_when_result_found() {
            when(searchService.by(query)).thenReturn(Either.right(searchResult));

            flow.tryRun();

            verify(io, times(1)).showResults(searchResult);
        }

        @Test
        void should_display_error_message_when_error_occurs() {
            when(searchService.by(query)).thenReturn(Either.left("Error"));

            flow.tryRun();

            verify(io, times(1)).show("Error");
        }
    }

    @Nested
    class Schema {
        @BeforeEach
        void before() {
            when(io.askForAction()).thenReturn(Action.SCHEMA);
        }

        @Test
        void should_prompt_for_entity_type() {
            flow.tryRun();

            verify(io).askForEntityType();
        }

        @Test
        void should_call_search_app_for_schema_by_entity_type() {
            var entityType = EntityType.USER;
            when(io.askForEntityType()).thenReturn(entityType);

            flow.tryRun();

            verify(searchService).schemaFor(entityType);
        }

        @Test
        void should_display_schema() {
            var entityType = EntityType.USER;
            when(io.askForEntityType()).thenReturn(entityType);
            when(searchService.schemaFor(entityType)).thenReturn("schema");

            flow.tryRun();

            verify(io).show("schema");
        }
    }


    @Nested
    class FlowControl {
        @BeforeEach
        void before() {
            when(searchService.by(any())).thenReturn(Either.right(searchResult));
        }

        @Test
        void should_dispose_terminal_when_user_choose_to_abort() {

            when(io.askForAction()).thenReturn(Action.SEARCH);
            when(io.askForContinue()).thenReturn(false);

            flow.tryRun();

            verify(io, times(1)).dispose();
        }

        @Test
        void should_restart_flow_when_user_choose_to_continue() {
            when(io.askForAction()).thenReturn(Action.SEARCH);
            when(io.askForContinue()).thenReturn(true, true, false);

            flow.tryRun();

            verify(io, times(3)).askForAction();
        }
    }
}
