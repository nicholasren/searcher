package ren.nicholas.searcher.app.service;

import io.vavr.collection.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ren.nicholas.searcher.app.service.controller.Query;
import ren.nicholas.searcher.app.service.model.EntityType;
import ren.nicholas.searcher.app.util.Fixture;

import static org.assertj.core.api.Assertions.assertThat;
import static ren.nicholas.searcher.app.service.SearchServiceLauncher.service;
import static ren.nicholas.searcher.app.util.Fixture.DomainData.PRESENTABLE_USER_SCHEMA;
import static ren.nicholas.searcher.search.impl.utils.Files.testResourcePathOf;
import static ren.nicholas.searcher.search.impl.utils.Fixture.*;

class SearcherFunctionalTest {
    private SearchService searchService;

    @BeforeEach
    void beforeAll() {
        searchService = service()
                .workingDir(testResourcePathOf("launcher_test_dirs/all_exists"))
                .launch()
                .get();
    }

    @Test
    void should_allow_to_search_users() {
        List<String> users = searchService.by(new Query(EntityType.USER, "_id", USER_ID_STRING)).get();
        assertThat(users.size()).isEqualTo(1);
        assertThat(users.head()).isEqualTo(Fixture.DomainData.PRESENTABLE_USER);
    }

    @Test
    void should_allow_to_search_organizations() {
        List<String> organizations = searchService.by(new Query(EntityType.ORGANIZATION, "_id", ORGANIZATION_ID_STRING)).get();

        assertThat(organizations.size()).isEqualTo(1);
        assertThat(organizations.head()).isEqualTo(Fixture.DomainData.PRESENTABLE_ORGANIZATION);
    }

    @Test
    void should_allow_to_search_tickets() {
        List<String> tickets = searchService.by(new Query(EntityType.TICKET, "_id", TICKET_ID_STRING)).get();

        assertThat(tickets.size()).isEqualTo(1);
        assertThat(tickets.head()).isEqualTo(Fixture.DomainData.PRESENTABLE_TICKET);
    }


    @Test
    void should_print_schema() {
        String schema = searchService.schemaFor(EntityType.USER);

        assertThat(schema).isEqualTo(PRESENTABLE_USER_SCHEMA);
    }
}
