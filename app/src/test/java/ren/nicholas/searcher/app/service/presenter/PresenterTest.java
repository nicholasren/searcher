package ren.nicholas.searcher.app.service.presenter;

import org.junit.jupiter.api.Test;
import ren.nicholas.searcher.app.service.model.Organization;
import ren.nicholas.searcher.app.service.model.User;
import ren.nicholas.searcher.app.util.Fixture;

import static java.lang.System.lineSeparator;
import static org.assertj.core.api.Assertions.assertThat;
import static ren.nicholas.searcher.app.util.Fixture.DomainData.*;
import static ren.nicholas.searcher.search.impl.utils.Fixture.DocumentData.USER_SCHEMA;

class PresenterTest {


    @Test
    void should_transfer_user_to_printable_format() {
        String users = new Presenter<User>().apply(DOMAIN_USER);
        assertThat(users).isEqualTo(PRESENTABLE_USER);
    }

    @Test
    void should_transfer_organization_to_printable_format() {
        String organizations = new Presenter<Organization>().apply(DOMAIN_ORGANIZATION);
        assertThat(organizations).isEqualTo(Fixture.DomainData.PRESENTABLE_ORGANIZATION);
    }

    @Test
    void should_transfer_schema_to_printable_format() {
        String userSchema = new Presenter<User>().show(USER_SCHEMA);
        assertThat(userSchema).isEqualTo(
                "_id" + lineSeparator() +
                "url" + lineSeparator() +
                "external_id" + lineSeparator() +
                "name" + lineSeparator() +
                "alias" + lineSeparator() +
                "created_at" + lineSeparator() +
                "active" + lineSeparator() +
                "verified" + lineSeparator() +
                "shared" + lineSeparator() +
                "locale" + lineSeparator() +
                "timezone" + lineSeparator() +
                "last_login_at" + lineSeparator() +
                "email" + lineSeparator() +
                "phone" + lineSeparator() +
                "signature" + lineSeparator() +
                "tags" + lineSeparator() +
                "organization_id" + lineSeparator() +
                "suspended" + lineSeparator() +
                "role");
    }
}