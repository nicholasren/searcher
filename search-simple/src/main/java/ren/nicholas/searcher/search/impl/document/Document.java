package ren.nicholas.searcher.search.impl.document;

import io.vavr.collection.List;
import io.vavr.collection.Seq;
import io.vavr.control.Option;
import ren.nicholas.searcher.search.impl.document.field.Field;

public class Document implements ren.nicholas.searcher.search.api.Document {

    private final Seq<Field> fields;

    public static DocumentBuilder builder() {
        return DocumentBuilder.builder();
    }

    @Override
    public String stringValueOf(final String name) {
        return valueOf(name).map(o -> (String) o).getOrElse("");
    }

    @Override
    public Long numericValueOf(final String name) {
        return valueOf(name).map(o -> (Long) o).getOrElse(0L);
    }

    @Override
    public Boolean booleanValueOf(final String name) {
        return valueOf(name).map(o -> (Boolean) o).getOrElse(false);
    }

    @Override
    public Seq<String> collectionValueOf(final String name) {
        return valueOf(name).map(o -> (Seq<String>) o).getOrElse(List.empty());
    }

    @Override
    public boolean match(final String name, final String value) {
        var matchedField = field(name);
        return matchedField.isDefined() && matchedField.get().match(value);
    }

    public Option<Field> field(final String name) {
        return fields.find(f -> f.name().equals(name));
    }

    private Option<Object> valueOf(final String name) {
        return field(name).map(Field::value);
    }

    Document(final Seq<Field> fields) {
        this.fields = fields;
    }
}
