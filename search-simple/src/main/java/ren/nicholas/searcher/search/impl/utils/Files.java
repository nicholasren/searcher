package ren.nicholas.searcher.search.impl.utils;

import java.nio.file.Path;
import java.nio.file.Paths;

public final class Files {
    public static String testResourcePathOf(final String name) {
        String projectRoot = System.getProperty("user.dir");
        Path path = Paths.get(projectRoot, "src", "test", "resources", name);
        return path.toString();
    }

    private Files() {
    }
}
