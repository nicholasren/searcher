package ren.nicholas.searcher.search.impl.document.field;

class StringField implements Field<String> {
    private final String name;
    private final String value;

    StringField(final String name, final String value) {
        this.name = name;
        this.value = value;
    }

    @Override
    public String name() {
        return name;
    }

    public String value() {
        return value;
    }

    @Override
    public boolean match(final String term) {
        return this.value.equals(term);
    }
}
