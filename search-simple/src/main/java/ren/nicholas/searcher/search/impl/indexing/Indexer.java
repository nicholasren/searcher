package ren.nicholas.searcher.search.impl.indexing;


import com.jsoniter.JsonIterator;
import com.jsoniter.spi.TypeLiteral;
import io.vavr.collection.List;
import io.vavr.control.Try;
import ren.nicholas.searcher.search.impl.document.Document;
import ren.nicholas.searcher.search.impl.document.field.Field;

import java.nio.file.Files;
import java.nio.file.Paths;

import static java.util.stream.Collectors.toList;


public class Indexer {

    private static final TypeLiteral<java.util.List<java.util.Map<String, Object>>> TYPE_LITERAL = new TypeLiteral<>() {
    };

    public Try<Index> load(final String name, final String filePath) {
        return from(filePath).map(documents -> new Index(name, documents));
    }

    private Try<List<Document>> from(final String documentFilePath) {
        return Try.of(() -> new String(Files.readAllBytes(Paths.get(documentFilePath))))
                .map(this::extract);
    }

    private List<Document> extract(final String json) {
        java.util.List<java.util.Map<String, Object>> records = JsonIterator.deserialize(json, TYPE_LITERAL);

        java.util.List<Document> documents = records.stream().map(record -> {
            var fields = List.ofAll(record.entrySet().stream().map(this::toField));
            return Document.builder().with(fields).create();
        }).collect(toList());
        return List.ofAll(documents);
    }

    private Field toField(final java.util.Map.Entry<String, Object> t) {
        return Field.of(t.getKey(), t.getValue());
    }
}
