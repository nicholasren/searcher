package ren.nicholas.searcher.search.impl.engine;

import io.vavr.collection.HashMap;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import io.vavr.collection.Seq;
import ren.nicholas.searcher.search.api.Document;
import ren.nicholas.searcher.search.api.Query;
import ren.nicholas.searcher.search.api.SearchEngine;
import ren.nicholas.searcher.search.impl.indexing.Index;

public class InMemorySearchEngine implements SearchEngine {

    private Map<String, Index> indices = HashMap.empty();

    InMemorySearchEngine(final Seq<Index> indices) {
        indices.forEach(this::register);
    }

    @Override
    public List<Document> search(final Query query) {
        return this.indices.get(query.getEntityType()).map(
                i -> i.by(query.getName(), query.getValue())
        ).getOrElse(List.empty());
    }

    private void register(final Index index) {
        this.indices = this.indices.put(index.getName(), index);
    }
}
