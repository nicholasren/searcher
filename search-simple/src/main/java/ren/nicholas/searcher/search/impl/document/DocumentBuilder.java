package ren.nicholas.searcher.search.impl.document;

import io.vavr.collection.List;
import io.vavr.collection.Seq;
import ren.nicholas.searcher.search.impl.document.field.Field;

public final class DocumentBuilder {
    private Seq<Field> fields = List.empty();

    private DocumentBuilder() {
    }

    public static DocumentBuilder builder() {
        return new DocumentBuilder();
    }

    public DocumentBuilder with(final Field field) {
        this.fields = this.fields.append(field);
        return this;
    }

    public DocumentBuilder with(final Seq<Field> fields) {
        this.fields = this.fields.appendAll(fields);
        return this;
    }

    public Document create() {
        return new Document(fields);
    }
}
