package ren.nicholas.searcher.search.impl.engine;

import io.vavr.collection.Map;
import io.vavr.collection.Seq;
import io.vavr.control.Option;
import io.vavr.control.Try;
import ren.nicholas.searcher.search.api.SearchEngine;
import ren.nicholas.searcher.search.impl.indexing.Index;
import ren.nicholas.searcher.search.impl.indexing.Indexer;

public final class SearchEngineLauncher {
    public static Try<SearchEngine> launch(final Map<String, String> config) {
        Indexer indexer = new Indexer();
        Seq<Try<Index>> indices = config.map(c -> indexer.load(c._1, c._2));

        Option<Try<Index>> failure = indices.find(Try::isFailure);
        if (failure.isDefined()) {
            return Try.failure(failure.get().getCause());
        } else {
            return Try.success(new InMemorySearchEngine(indices.map(Try::get)));
        }
    }

    private SearchEngineLauncher() {
    }
}
