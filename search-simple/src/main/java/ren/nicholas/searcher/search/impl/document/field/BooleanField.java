package ren.nicholas.searcher.search.impl.document.field;

import io.vavr.control.Try;

public class BooleanField implements Field<Boolean> {
    private final String name;
    private final Boolean value;

    BooleanField(final String name, final Boolean value) {
        this.name = name;
        this.value = value;
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public Boolean value() {
        return value;
    }

    @Override
    public boolean match(final String term) {
        return Try.of(() -> Boolean.parseBoolean(term))
                .map(value::equals)
                .getOrElse(false);
    }
}
