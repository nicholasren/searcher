package ren.nicholas.searcher.search.impl.document.field;

import io.vavr.collection.Seq;

import static com.google.common.base.Strings.isNullOrEmpty;

class StringCollectionField implements Field<Seq<String>> {

    private final String name;
    private final Seq<String> value;

    StringCollectionField(final String name, final Seq<String> value) {
        this.name = name;
        this.value = value;
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public Seq<String> value() {
        return value;
    }

    @Override
    public boolean match(final String term) {
        return emptyMatches(term) || value.contains(term);
    }

    private boolean emptyMatches(final String term) {
        return value.isEmpty() && isNullOrEmpty(term);
    }
}
