package ren.nicholas.searcher.search.impl.document.field;

import io.vavr.collection.List;

public interface Field<T> {

    String name();

    T value();

    boolean match(String query);

    static <T> Field<T> of(final String name, final T value) {
        if (value instanceof Boolean) {
            return (Field<T>) new BooleanField(name, (Boolean) value);
        } else if (value instanceof Number) {
            return (Field<T>) new LongField(name, ((Number) value).longValue());
        } else if (value instanceof Iterable) {
            return (Field<T>) new StringCollectionField(name, List.ofAll((Iterable<String>) value));
        } else {
            return (Field<T>) new StringField(name, (String) value);
        }
    }
}
