package ren.nicholas.searcher.search.impl.indexing;

import io.vavr.collection.List;
import ren.nicholas.searcher.search.impl.document.Document;

public class Index {
    private final String name;
    private final List<Document> documents;

    public Index(final String name, final List<Document> documents) {
        this.name = name;
        this.documents = documents;
    }

    public List<ren.nicholas.searcher.search.api.Document> by(final String name, final String value) {
        return documents.filter(document -> document.match(name, value))
                .map(x -> x);
    }

    public String getName() {
        return name;
    }

    public int size() {
        return this.documents.size();
    }

    public Document head() {
        return this.documents.head();
    }
}
