package ren.nicholas.searcher.search.impl.document.field;

import io.vavr.control.Try;

public class LongField implements Field<Long> {
    private final String name;
    private final Long value;

    LongField(final String name, final Long value) {
        this.name = name;
        this.value = value;
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public Long value() {
        return value;
    }

    @Override
    public boolean match(final String term) {
        return Try.of(() -> Long.parseLong(term))
                .map(value::equals)
                .getOrElse(false);
    }
}
