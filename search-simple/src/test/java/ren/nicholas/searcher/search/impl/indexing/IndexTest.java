package ren.nicholas.searcher.search.impl.indexing;

import io.vavr.collection.List;
import org.junit.jupiter.api.Test;
import ren.nicholas.searcher.search.api.Document;
import ren.nicholas.searcher.search.impl.utils.Fixture;

import static org.assertj.core.api.Assertions.assertThat;
import static ren.nicholas.searcher.search.impl.utils.Fixture.USER_ID_STRING;

class IndexTest {

    @Test
    void should_return_matched_documents() {
        Index index = new Index("USER", List.of(Fixture.DocumentData.DOCUMENT_USER));

        List<Document> actual = index.by("_id", USER_ID_STRING);

        assertThat(actual.size()).isEqualTo(1);
    }
}