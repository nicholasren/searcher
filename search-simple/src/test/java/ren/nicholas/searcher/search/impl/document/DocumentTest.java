package ren.nicholas.searcher.search.impl.document;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import ren.nicholas.searcher.search.impl.document.field.Field;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static ren.nicholas.searcher.search.impl.document.Document.builder;
import static ren.nicholas.searcher.search.impl.document.field.Field.of;

class DocumentTest {

    @Nested
    @DisplayName("empty field value matching")
    class EmptyFieldValueMatching {

        @Test
        void should_be_true_when_empty_string_value_matches() {
            Document document = builder().with(of("string_field", "")).create();
            assertThat(document.match("string_field", "")).isEqualTo(true);
        }

        @Test
        void should_be_true_when_empty_collection_value_matches() {
            Document document = builder().with(of("collection_field", List.of())).create();
            assertThat(document.match("collection_field", "")).isEqualTo(true);
        }
    }

    @Nested
    @DisplayName("string field matching")
    class StringFieldMatching {
        private Document document = builder()
                .with(of("string_field", "value"))
                .create();

        @Test
        void should_be_false_when_has_no_field_with_same_value() {
            assertThat(document.match("string_field", "not_matching")).isEqualTo(false);
        }

        @Test
        void should_be_false_when_has_no_field_with_same_name() {
            assertThat(document.match("other_field", "text")).isEqualTo(false);
        }

        @Test
        void should_be_true_when_has_field_with_same_value() {
            assertThat(document.match("string_field", "value")).isEqualTo(true);
        }
    }

    @Nested
    @DisplayName("boolean field matching")
    class BooleanFieldMatching {
        private Document document = builder()
                .with(of("boolean_field", true))
                .create();

        @Test
        void should_be_false_when_has_no_field_with_same_name() {
            assertThat(document.match("other_field", "true")).isEqualTo(false);
        }

        @Test
        void should_be_false_when_has_no_field_with_same_value() {
            assertThat(document.match("boolean_field", "false")).isEqualTo(false);
        }

        @Test
        void should_be_true_when_has_field_with_same_value() {
            assertThat(document.match("boolean_field", "true")).isEqualTo(true);
        }

    }


    @Nested
    @DisplayName("number field matching")
    class NumberFieldMatching {
        private Document document = builder()
                .with(of("number_field", 10L))
                .create();

        @Test
        void should_be_false_when_has_no_field_with_same_name() {
            assertThat(document.match("other_field", "10")).isEqualTo(false);
        }

        @Test
        void should_be_false_when_has_no_field_with_same_value() {
            assertThat(document.match("number_field", "-1")).isEqualTo(false);
        }

        @Test
        void should_be_true_when_has_field_with_same_value() {
            assertThat(document.match("number_field", "10")).isEqualTo(true);
        }
    }

    @Nested
    @DisplayName("collection field matching")
    class CollectionFieldMatching {
        private Document document = builder()
                .with(of("collection_field", List.of("v1", "v2", "v3")))
                .create();

        @Test
        void should_be_false_when_has_no_field_with_same_name() {
            assertThat(document.match("other_field", "1")).isEqualTo(false);
        }

        @Test
        void should_be_false_when_has_field_dose_not_contain_the_value() {
            assertThat(document.match("collection_field", "v0")).isEqualTo(false);
        }

        @Test
        void should_be_true_when_has_field_contains_the_value() {
            assertThat(document.match("collection_field", "v1")).isEqualTo(true);
            assertThat(document.match("collection_field", "v2")).isEqualTo(true);
            assertThat(document.match("collection_field", "v3")).isEqualTo(true);
        }

    }

}