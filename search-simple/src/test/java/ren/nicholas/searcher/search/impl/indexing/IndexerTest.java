package ren.nicholas.searcher.search.impl.indexing;


import io.vavr.control.Try;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import ren.nicholas.searcher.search.impl.document.Document;
import ren.nicholas.searcher.search.impl.document.field.Field;

import static org.assertj.core.api.Assertions.assertThat;
import static ren.nicholas.searcher.search.impl.utils.Files.testResourcePathOf;

class IndexerTest {

    private Indexer loader = new Indexer();

    @Nested
    class LoadDocument {
        @Test
        void should_load_index_when_no_exceptions_raised() {
            Try<Index> index = loader.load("USER", testResourcePathOf("documents/two_records.json"));

            assertThat(index).isInstanceOf(Try.Success.class);
            assertThat(index.get().size()).isEqualTo(2);
        }

        @Test
        void should_return_failure_when_file_is_absent() {
            Try<Index> index = loader.load("USER", "documents/not_exists.json");

            assertThat(index).isInstanceOf(Try.Failure.class);
        }

    }

    @DisplayName("Load document fields")
    @Nested
    class LoadDocumentFields {

        private Document document;

        @BeforeEach
        void before() {
            document = loader.load("USER", testResourcePathOf("documents/with_all_types_of_fields.json"))
                    .get().head();
        }

        @Test
        void should_load_string_value_as_string() {
            Field field = document.field("text_field").get();

            document.stringValueOf("text_field");

            assertThat(field.value()).isEqualTo("text");
        }

        @Test
        void should_load_numeric_value_as_long() {
            assertThat(document.numericValueOf("numeric_field")).isEqualTo(10);
        }

        @Test
        void should_load_boolean_value_as_string() {
            assertThat(document.booleanValueOf("boolean_field")).isEqualTo(true);
        }

        @Test
        void should_load_collection_values_as_collection() {
            assertThat(document.collectionValueOf("collection_field")).contains("v1", "v2", "v3");
        }
    }
}
