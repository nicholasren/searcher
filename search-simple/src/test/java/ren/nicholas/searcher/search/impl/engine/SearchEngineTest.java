package ren.nicholas.searcher.search.impl.engine;


import io.vavr.collection.HashMap;
import io.vavr.collection.List;
import io.vavr.control.Try;
import org.junit.jupiter.api.Test;
import ren.nicholas.searcher.search.api.Document;
import ren.nicholas.searcher.search.api.Query;
import ren.nicholas.searcher.search.api.SearchEngine;

import static org.assertj.core.api.Assertions.assertThat;
import static ren.nicholas.searcher.search.impl.engine.SearchEngineLauncher.launch;
import static ren.nicholas.searcher.search.impl.utils.Files.testResourcePathOf;

class SearchEngineTest {
    private static final String USER = "USER";
    private static final String ORGANIZATION = "ORGANIZATION";

    @Test
    void should_launch_app_when_no_error() {
        Try<SearchEngine> app = launch(HashMap.of(USER, testResourcePathOf("users.json")));

        assertThat(app.isSuccess()).isTrue();
    }

    @Test
    void should_return_search_result() {
        Try<SearchEngine> app = launch(HashMap.of(USER, testResourcePathOf("users.json")));

        List<Document> documents = app.get().search(new Query(USER, "_id", "1"));

        assertThat(documents.size()).isEqualTo(1);
    }

    @Test
    void should_return_cause_on_any_failure() {
        Try<SearchEngine> app = launch(HashMap.of(
                USER, testResourcePathOf("users.json"),
                ORGANIZATION, "not_exists.json"
        ));

        assertThat(app.isFailure()).isTrue();
        assertThat(app.getCause()).isInstanceOf(Throwable.class);
    }
}