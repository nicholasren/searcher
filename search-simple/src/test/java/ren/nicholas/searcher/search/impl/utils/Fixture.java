package ren.nicholas.searcher.search.impl.utils;

import io.vavr.Tuple;
import io.vavr.Tuple2;
import io.vavr.collection.HashMap;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import io.vavr.collection.Seq;
import ren.nicholas.searcher.search.impl.document.Document;
import ren.nicholas.searcher.search.impl.document.DocumentBuilder;
import ren.nicholas.searcher.search.impl.document.field.Field;

public class Fixture {
    private static final long ORGANIZATION_ID_LONG = 101l;
    private static final long USER_ID_LONG = 1l;
    public static final String USER_ID_STRING = String.valueOf(USER_ID_LONG);
    public static final String ORGANIZATION_ID_STRING = String.valueOf(ORGANIZATION_ID_LONG);
    public static final String TICKET_ID_STRING = "436bf9b0-1147-4c0a-8439-6f79833bff5b";

    private static List<Tuple2<String, Object>> USER_VALUE_ENTRIES = List.of(
            Tuple.of("_id", USER_ID_LONG),
            Tuple.of("url", "http://a.b.c"),
            Tuple.of("external_id", "1-b-c-d"),
            Tuple.of("name", "Prince Hinton"),
            Tuple.of("alias", "Miss Dana"),
            Tuple.of("created_at", "2016-04-18T11:05:43 -10:00"),
            Tuple.of("active", true),
            Tuple.of("verified", false),
            Tuple.of("shared", false),
            Tuple.of("locale", "zh-CN"),
            Tuple.of("timezone", "Samoa"),
            Tuple.of("last_login_at", "2013-05-01T01:18:48 -10:00"),
            Tuple.of("email", "danahinton@flotonic.com"),
            Tuple.of("phone", "9064-433-892"),
            Tuple.of("signature", "Don't Worry Be Happy!"),
            Tuple.of("tags", List.of("Davenport", "Cherokee", "Summertown", "Clinton")),
            Tuple.of("organization_id", ORGANIZATION_ID_LONG),
            Tuple.of("suspended", false),
            Tuple.of("role", "agent"));

    private static final List<Tuple2<String, Object>> ORG_VALUE_ENTRIES = List.of(
            Tuple.of("_id", ORGANIZATION_ID_LONG),
            Tuple.of("url", "http://initech.zendesk.com/api/v2/organizations/101.json"),
            Tuple.of("external_id", "9270ed79-35eb-4a38-a46f-35725197ea8d"),
            Tuple.of("name", "Enthaze"),
            Tuple.of("domain_names", List.of("kage.com", "ecratic.com", "endipin.com", "zentix.com")),
            Tuple.of("created_at", "2016-05-21T11:10:28 -10:00"),
            Tuple.of("details", "MegaCorp"),
            Tuple.of("shared_tickets", false),
            Tuple.of("tags", List.of("Fulton", "West", "Rodriguez", "Farley"))
    );

    public static Map<String, Object> USER_VALUES = HashMap.ofEntries(USER_VALUE_ENTRIES);
    public static Map<String, Object> ORG_VALUES = HashMap.ofEntries(ORG_VALUE_ENTRIES);

    public static class DocumentData {
        public static final Document DOCUMENT_USER = DocumentBuilder
                .builder()
                .with(USER_VALUES.map(t -> Field.of(t._1, t._2)))
                .create();

        public static final Seq<String> USER_SCHEMA = USER_VALUE_ENTRIES.map(Tuple2::_1);

        public static final Document DOCUMENT_ORGANIZATION = DocumentBuilder
                .builder()
                .with(ORG_VALUES.map(t -> Field.of(t._1, t._2)))
                .create();
    }
}
