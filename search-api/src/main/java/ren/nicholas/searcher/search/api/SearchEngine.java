package ren.nicholas.searcher.search.api;

import io.vavr.collection.List;

public interface SearchEngine {
    List<Document> search(Query query);
}
