package ren.nicholas.searcher.search.api;

import io.vavr.collection.Seq;

public interface Document {

    String stringValueOf(String name);

    Long numericValueOf(String name);

    Boolean booleanValueOf(String name);

    Seq<String> collectionValueOf(String name);

    boolean match(String name, String value);
}
