package ren.nicholas.searcher.search.api;

public class Query {

    private final String entityType;
    private final String value;
    private final String name;

    public Query(final String entityType, final String name, final String value) {
        this.entityType = entityType;
        this.value = value;
        this.name = name;
    }

    public String getEntityType() {
        return this.entityType;
    }

    public String getValue() {
        return value;
    }

    public String getName() {
        return name;
    }
}
