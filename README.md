# Searcher

### Prerequisites
- Java 11
- Gradle 5.4
- A directory has been created which contains the following files:
    - users.json
    - organizations.json
    - tickets.json

### Getting Started
- Open terminal an change direcroty to project root.
- Run `./scripts/deploy.sh` to build and deploy the application.
- Run `./scripts/run.sh` for usage instruction.


### Assumptions

#### Search Engine

1. I assumed that open source indexing tools should not be used in this challenge, so I decided to implement a very simple in-memory, field-based document search engine, see module "search-simple".
the `search-simple` module has minimal capabilities, only the following `Long`,  `Boolean`,  `String` and `Iterable<String>` field are supported.
A `search-api` module is created to cope with the possibility of a switch to different search implementation.  

2. Assuming these JSON files are static while app is running, the app will run a full indexing of these files during startup, increamental indexing is not supported.

#### Searcher App

1. The search app is only considered functional when all three json files are provided in a working directory, hence any malformed data in these json files will prevent the app from starting.

2. Record might be missing for an associated user/organization/ticket id in the JSON files, an "Unknown" name will be displayed for these associations. 