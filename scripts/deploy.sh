#!/usr/bin/env bash

./gradlew clean build distZip
cd ./app/build/distributions/ && unzip zendesk-searcher-1.0.zip