#!/usr/bin/env bash


if [ $# -eq 0 ];
then
  echo "Usage: ./scripts/run.sh <work-directory>
  Example usage: ./scripts/run.sh ~/dev/data"
  exit 1
fi

workDir="${1?work-directory must be provided}"

cd ./app/build/distributions/zendesk-searcher-1.0 && ./bin/zendesk-searcher $workDir